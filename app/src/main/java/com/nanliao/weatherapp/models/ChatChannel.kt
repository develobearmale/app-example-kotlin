package com.nanliao.weatherapp.models

data class ChatChannel(val userIds: MutableList<String>) {
    constructor() : this(mutableListOf())
}