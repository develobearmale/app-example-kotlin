package com.nanliao.weatherapp.models.Util

import android.content.Context
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.nanliao.weatherapp.models.*
import com.xwray.groupie.kotlinandroidextensions.Item
import java.lang.NullPointerException

object FirestoreUtil {

    private val firestoreInstance: FirebaseFirestore by lazy {
        FirebaseFirestore.getInstance()
    }

    private val currentUserDocRef: DocumentReference
        get() = firestoreInstance.document(
            "users/${FirebaseAuth.getInstance().currentUser?.uid
                ?: throw NullPointerException("UID is null") as Throwable}"
        )

    private val chatChannelCollectionRef = firestoreInstance.collection("chatChannels")

    fun initCurrentUserFirstTime(onComplete: () -> Unit) {
        currentUserDocRef.get().addOnSuccessListener { documentSnapshot ->
            if (!documentSnapshot.exists()) {

                val newUser = User(
                    FirebaseAuth.getInstance().currentUser?.displayName ?: "",
                    "",
                    null,
                    mutableListOf()
                )
                currentUserDocRef.set(newUser).addOnSuccessListener {
                    onComplete()
                }
            }
            else
                onComplete()
        }
    }

    fun updateCurrentUser(name:String = "",bio:String="",profilePicturePath:String? = null){
        val userFieldMap = mutableMapOf<String,Any>()
        if(name.isNotBlank()) userFieldMap["name"] = name
        if(bio.isNotBlank()) userFieldMap["bio"] = bio
        if(profilePicturePath!=null) userFieldMap["profilePicturePath"] = profilePicturePath
        currentUserDocRef.update(userFieldMap)

    }

    fun getCurrentUser(onComplete: (User) -> Unit) {
        currentUserDocRef.get()
            .addOnSuccessListener {
                onComplete(it.toObject(User::class.java)!!)
            }
    }

    fun addUserListener(context:Context, onListen: (List<Item>) -> Unit): ListenerRegistration{
        return firestoreInstance.collection("users")
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException!=null) {
                    Log.e("FIRESTORE", "Users listener error.", firebaseFirestoreException)
                    return@addSnapshotListener
                }
                val items = mutableListOf<Item>()
                querySnapshot!!.documents.forEach {
                    if(it.id != FirebaseAuth.getInstance().currentUser?.uid)
                        items.add(Person(it.toObject(User::class.java)!!, it.id,context))
                }
                onListen(items)
            }
    }

    fun removeListener(registration: ListenerRegistration) = registration.remove()

    fun getOnCreateChatChannel(otherUserId: String, onComplete: (channelId: String) -> Unit){
        currentUserDocRef.collection("engagedChatChannels").document(otherUserId).get().addOnSuccessListener {
            if(it.exists()){
                onComplete(it["channelId"] as String)
                return@addOnSuccessListener
            }

            val currentUserID = FirebaseAuth.getInstance().currentUser!!.uid
            val newChannel = chatChannelCollectionRef.document()
            newChannel.set(ChatChannel(mutableListOf(currentUserID, otherUserId)))

            currentUserDocRef
                .collection("engagedChatChannels")
                .document(otherUserId)
                .set(mapOf("channelId" to newChannel.id))

            firestoreInstance.collection("users").document(otherUserId)
                .collection("engagedChatChannels")
                .document(currentUserID)
                .set(mapOf("channelId" to newChannel.id))

            onComplete(newChannel.id)
        }
    }

    fun addChatMessagesListener(channelId:String,context:Context,
                                onListen:(List<Item>)->Unit): ListenerRegistration{

        return chatChannelCollectionRef.document(channelId).collection("messages")
            .orderBy("time")
            .addSnapshotListener{querySnapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException!=null){
                    Log.e("FIRESTORE","ChatMessagesListener error.",firebaseFirestoreException)
                    return@addSnapshotListener
                }

                val items = mutableListOf<Item>()
                querySnapshot!!.documents.forEach {
                    if(it["type"]==MessageType.TEXT)
                        items.add(TextMessageItem(it.toObject(TextMessage::class.java)!!,context))
                    else
                        items.add(ImageMessageItem(it.toObject(ImageMessage::class.java)!!, context))
                    return@forEach
                }
                onListen(items)

            }
    }

    fun sendMessage(message:Message, channelId:String){
        chatChannelCollectionRef.document(channelId)
            .collection("messages")
            .add(message)
    }

    //region FCM

    fun getFCMRegistrationTokens(onComplete: (tokens: MutableList<String>) -> Unit){
        currentUserDocRef.get().addOnSuccessListener {
            val user = it.toObject(User::class.java)
            onComplete(user!!.registrationTokens)
        }
    }

    fun setFCMRegistrationTokens(registrationTokens:MutableList<String>){
        currentUserDocRef.update(mapOf("registrationTokens" to registrationTokens))
    }

    //endregion FCM

}