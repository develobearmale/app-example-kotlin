package com.nanliao.weatherapp.presenters.cloud

import com.google.gson.GsonBuilder
import com.nanliao.weatherapp.models.Hero
import com.nanliao.weatherapp.presenters.network.RestApi
import com.nanliao.weatherapp.presenters.remote.HeroRemote
import com.nanliao.weatherapp.presenters.remote.entitiesResponse.HeroResponse
import io.reactivex.Observable

import okhttp3.*
import java.io.IOException
import java.lang.Exception

class CloudHeroe {

    companion object {
        val request = Request.Builder().url(RestApi.REST_PATH_HEROE).build()
        val client = OkHttpClient()
        var mutableListHeroRemote: MutableList<HeroRemote> = mutableListOf()
        private var listHeroes: List<Hero> = ArrayList()
        fun getHeroes(): MutableList<Hero> {
            var listHeroes: MutableList<Hero> = mutableListOf()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {

                }

                override fun onResponse(call: Call, response: Response) {
                    val body = response?.body()?.string()
                    val gson = GsonBuilder().create()
                    mutableListHeroRemote = gson.fromJson(body, Array<HeroRemote>::class.java).toMutableList()
                    //var listHeroRemote:List<HeroRemote> = gson.fromJson(body,Array<HeroRemote>::class.java).toList()
                    //any<OnDataListListener<*>>(OnDataListListener::class.java)

                    listHeroes = HeroResponse.toHeroes(mutableListHeroRemote)
                }
            })
            return listHeroes
        }

        fun getHeroRemote(): Observable<MutableList<Hero>> {
            return Observable.create { emitter ->
                val url = "https://game-database-24015.firebaseio.com/steam/dota2/heroes.json"
                val request = Request.Builder().url(url).build()
                val client = OkHttpClient()
                client.newCall(request).enqueue(object : Callback {
                    override fun onResponse(call: Call?, response: Response?) {
                        val body = response?.body()?.string()
                        val gson = GsonBuilder().create()
                        val homeFeed: MutableList<HeroRemote> =
                            gson.fromJson(body, Array<HeroRemote>::class.java).toMutableList()
                        emitter.onNext(HeroResponse.toHeroes(homeFeed))
                        emitter.onComplete()
                    }

                    override fun onFailure(call: Call?, e: IOException?) {
                        emitter.onError(Exception("Failed to execute request"))
                    }
                })
            }
        }
    }
}