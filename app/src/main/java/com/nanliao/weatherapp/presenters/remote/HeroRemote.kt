package com.nanliao.weatherapp.presenters.remote


import java.io.Serializable

class HeroRemote(
    id: String,
    localized_name: String,
    name: String,
    url_full_portrait: String,
    url_large_portrait: String,
    url_small_portrait: String,
    url_vertical_portrait: String
) : Serializable {

    var id: String = ""
    var localized_name: String = ""
    var name: String = ""
    var url_full_portrait: String = ""
    var url_large_portrait: String = ""
    var url_small_portrait: String = ""
    var url_vertical_portrait: String = ""


    init {
        this.id = id
        this.localized_name = localized_name
        this.name = name
        this.url_full_portrait = url_full_portrait
        this.url_large_portrait = url_large_portrait
        this.url_small_portrait = url_small_portrait
        this.url_vertical_portrait = url_vertical_portrait
    }
}


