package com.nanliao.weatherapp.presenters.remote.entitiesResponse
import com.nanliao.weatherapp.models.Hero;
import com.nanliao.weatherapp.presenters.remote.HeroRemote

class HeroResponse {

    companion object{
        fun toHeroes(heroEntities:MutableList<HeroRemote>): MutableList<Hero> {
            var heroes:MutableList<Hero> = ArrayList()
            if(heroes!=null){
                for(objHeroRemote in heroEntities){
                    heroes.  add(toHero(objHeroRemote))
                }
            }
            return heroes
        }

        fun toHero(heroEntity:HeroRemote): Hero {
            var hero: Hero = Hero(heroEntity.localized_name,heroEntity.name,heroEntity.url_full_portrait)
            /*if(hero!=null){
                hero.description = heroEntity.localized_name
                hero.name = heroEntity.name
                hero.image = heroEntity.url_full_portrait
            }*/
            return hero!!

        }
    }
}