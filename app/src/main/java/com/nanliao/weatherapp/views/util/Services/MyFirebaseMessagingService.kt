package com.nanliao.weatherapp.views.util.Services

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService: FirebaseMessagingService(){

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        if(remoteMessage?.notification!=null){
            Log.d("FCM","FCM message received!")
        }

    }

}