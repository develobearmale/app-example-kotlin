package com.nanliao.weatherapp.views.activities

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
//import com.google.firebase.auth.FirebaseAuth
//import com.firebase.ui.auth.AuthUI
//com.firebase.ui.auth.ErrorCodes
//import com.firebase.ui.auth.IdpResponse
import com.google.firebase.iid.FirebaseInstanceId
import com.nanliao.weatherapp.R
import com.nanliao.weatherapp.models.Util.FirestoreUtil
import com.nanliao.weatherapp.views.util.Services.MyFirebaseInstanceIDService
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class SignInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private val RC_SIGN_IN = 1

    /*private val sigInProviders =
            listOf(AuthUI.IdpConfig.EmailBuilder()
                .setAllowNewAccounts(true)
                .setRequireName(true)
                .build())*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        auth = FirebaseAuth.getInstance()


        btn_login_chat_firebase.setOnClickListener {
            /*val intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(this.sigInProviders)
                .setLogo(R.drawable.ic_icons8_firebase)
                .build()*/

            auth.signInWithEmailAndPassword(tv_email_firebase.text.toString(), tv_password_firebase.text.toString())
                .addOnCompleteListener(this) { task ->

                    if (task.isSuccessful) {
                        Log.d(
                            "SUCCESS",
                            "signInWithEmail:success " + task.result!!.user.uid + " " + task.result!!.user.displayName
                        )
                    }

                }


            val objIntent:Intent = Intent(applicationContext,ChatActivity::class.java)
            startActivity(objIntent)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if ( requestCode == RC_SIGN_IN){
            //val response = IdpResponse.fromResultIntent(data)

            if(resultCode == Activity.RESULT_OK){
                val progressDialog = indeterminateProgressDialog("Setting up your account")

                FirestoreUtil.initCurrentUserFirstTime {
                    startActivity(intentFor<ChatActivity>().newTask().clearTask())

                    val registrationToken = FirebaseInstanceId.getInstance().token
                    MyFirebaseInstanceIDService.addTokenToFirestore(registrationToken)

                    progressDialog.dismiss()
                }

            }
            else if(resultCode== Activity.RESULT_CANCELED){
                /*if(response == null) return

                when(response.error?.errorCode){
                    ErrorCodes.NO_NETWORK ->
                        longSnackbar(sigIn_constraint_layout, "No  network")
                    ErrorCodes.UNKNOWN_ERROR ->
                        longSnackbar(sigIn_constraint_layout, "Unknown  network")
                }*/
            }


        }
    }


}
