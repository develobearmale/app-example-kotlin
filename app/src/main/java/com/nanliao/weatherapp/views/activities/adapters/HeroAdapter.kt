package com.nanliao.weatherapp.views.activities.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.nanliao.weatherapp.R
import com.nanliao.weatherapp.models.Hero
import com.nanliao.weatherapp.presenters.remote.HeroRemote
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recyclerview_item_row.view.*

class HeroAdapter : RecyclerView.Adapter<HeroAdapter.HeroHolder>(), Filterable {

    var heroFeed: MutableList<Hero> = arrayListOf()
    var heroFeedSearched: MutableList<Hero> = arrayListOf()

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val constraint = constraint.toString()
                if (constraint.isEmpty()) {
                    heroFeedSearched = heroFeed
                } else {
                    val filteredList = ArrayList<Hero>()
                    for (row in heroFeed) {
                        if ((row.name.toLowerCase().contains(constraint.toLowerCase()))) {
                            filteredList.add(row)
                        }
                    }
                    heroFeedSearched = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = heroFeedSearched
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                heroFeedSearched = results?.values as ArrayList<Hero>
                notifyDataSetChanged()
            }

        }

    }

    class HeroHolder(private val view: View) : RecyclerView.ViewHolder(view) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val cellForRow = layoutInflater.inflate(R.layout.recyclerview_item_row, parent, false)
        return HeroHolder(cellForRow)
    }

    override fun getItemCount() = heroFeedSearched.size

    override fun onBindViewHolder(holder: HeroHolder, position: Int) {
        val itemHeroes = heroFeedSearched[position]
        holder?.itemView?.hero_name_textView?.text = itemHeroes.name
        holder?.itemView?.hero_progress_bar?.visibility = View.VISIBLE
        Picasso.with(holder?.itemView?.context).load(itemHeroes.description)
            .into(holder?.itemView?.hero_image_imageView, object : Callback {
                override fun onSuccess() {
                    if (holder?.itemView?.hero_progress_bar != null) {
                        holder?.itemView?.hero_progress_bar?.visibility = View.GONE
                    }
                }

                override fun onError() {}
            })
    }


}

