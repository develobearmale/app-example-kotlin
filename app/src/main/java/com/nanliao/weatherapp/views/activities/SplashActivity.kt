package com.nanliao.weatherapp.views.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.startActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(FirebaseAuth.getInstance().currentUser == null)
           // startActivity(Intent(applicationContext,SignInActivity::class.java))
            startActivity<SignInActivity>()
        else
           // startActivity(Intent(applicationContext,ChatActivity::class.java))
        startActivity<ChatActivity>()
        finish()
    }
}
