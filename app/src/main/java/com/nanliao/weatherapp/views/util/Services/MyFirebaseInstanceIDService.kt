package com.nanliao.weatherapp.views.util.Services

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
//import com.google.firebase.iid.FirebaseInstanceIdService
import com.nanliao.weatherapp.models.Util.FirestoreUtil

class MyFirebaseInstanceIDService /*: FirebaseInstanceIdService()*/ {

    fun onTokenRefresh() {
        val newRegistrationToken = FirebaseInstanceId.getInstance().token

        if(FirebaseAuth.getInstance().currentUser != null)
            addTokenToFirestore(newRegistrationToken)
    }

    companion object{
        fun addTokenToFirestore(newRegistrationToken:String?){
            if(newRegistrationToken==null)throw NullPointerException("FCM Token is null")

            FirestoreUtil.getFCMRegistrationTokens { tokens ->
                if(tokens.contains(newRegistrationToken))
                    return@getFCMRegistrationTokens

                tokens.add(newRegistrationToken)
                FirestoreUtil.setFCMRegistrationTokens(tokens)
            }
        }
    }

}