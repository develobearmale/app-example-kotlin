package com.nanliao.weatherapp.views.activities

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.MotionEvent
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.nanliao.weatherapp.R
import com.nanliao.weatherapp.views.util.Interfaces.Orientation
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


private lateinit var auth: FirebaseAuth

class LoginActivity : AppCompatActivity(), Orientation {

    override fun changueOrientation() {
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        auth = FirebaseAuth.getInstance()

        super.onCreate(savedInstanceState)
        changueOrientation()
        setContentView(R.layout.activity_login)
        var objActionBar: ActionBar? = supportActionBar
        objActionBar?.hide()
        var secondaryConstraintLayout: ConstraintLayout = findViewById(R.id.id_secondaryConstraintLayout)
        var tvRecoverPassword = findViewById<TextView>(R.id.tv_recover_password)
        val btn_login = findViewById<Button>(R.id.btn_login_mapro)

        var anim = AnimationUtils.loadAnimation(applicationContext, R.anim.slide)
        secondaryConstraintLayout.startAnimation(anim)

        tvRecoverPassword.setOnClickListener {
            withEditText()
        }

        btn_login.setOnClickListener {
            /*
            val objIntent:Intent = Intent(applicationContext,MainActivity::class.java)
            startActivity(objIntent)*/
            auth.signInWithEmailAndPassword(tv_email.text.toString(), tv_password.text.toString())
                .addOnCompleteListener(this) { task ->

                    if (task.isSuccessful) {
                        Log.d(
                            "SUCCESS",
                            "signInWithEmail:success " + task.result!!.user.uid + " " + task.result!!.user.displayName
                        )

                    } else {
                        Log.d("SUCCESS", "signInWithEmail:failure")
                        Toast.makeText(
                            this@LoginActivity, "Aut. Failed",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

        }

    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    fun withEditText() {
        val builder = AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
        val inflater = layoutInflater
        builder.setTitle("Recuperar contraseña")
        val dialogLayout = inflater.inflate(R.layout.alert_edit_text, null)
        val editText = dialogLayout.findViewById<EditText>(R.id.et_alert_forget_password)
        builder.setView(dialogLayout)
        builder.setPositiveButton("Enviar") { dialogInterface, i ->
            Toast.makeText(applicationContext, "EditText is ${editText.text}", Toast.LENGTH_SHORT).show()
        }
        builder.show()
    }

}
