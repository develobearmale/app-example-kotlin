package com.nanliao.weatherapp.views.activities

import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.nanliao.weatherapp.R
import com.nanliao.weatherapp.views.util.Interfaces.ActionBar
import com.nanliao.weatherapp.views.util.Interfaces.Orientation
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ActionBar, Orientation {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changueOrientation()
        setContentView(R.layout.activity_main)
        
        configureActionBar(
            resources.getString(R.string.mainActivity_name),
            ColorDrawable(resources.getColor(R.color.primary))
        )

        main_btn_heroes.setOnClickListener{
            startActivity(Intent(applicationContext,DotaActivity::class.java))
        }

        main_btn_chat.setOnClickListener{
            startActivity(Intent(applicationContext,ChatActivity::class.java))
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun configureActionBar(title: String, colorBar: ColorDrawable) {
        val actionbar = supportActionBar
        actionbar!!.title = title
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setBackgroundDrawable(colorBar)
    }

    override fun changueOrientation() {
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }



}
