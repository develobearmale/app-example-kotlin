package com.nanliao.weatherapp.views.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDialogFragment
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.nanliao.weatherapp.R
import com.nanliao.weatherapp.views.fragments.PeopleFragment
import com.nanliao.weatherapp.views.fragments.ProfileFragment
import com.nanliao.weatherapp.views.util.Interfaces.ActionBar
import com.nanliao.weatherapp.views.util.Interfaces.Orientation
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.frameLayout

class ChatActivity : AppCompatActivity(), ActionBar, Orientation {

    private lateinit var auth: FirebaseAuth
    var currentUser = "";

    public override fun onStart() {
        super.onStart()
        Log.d("MIGUEL ", "HOLA")
        // Check if user is signed in (non-null) and update UI accordingly.
        if (auth != null && auth.currentUser != null){
            currentUser  = auth.currentUser!!.email.toString()
            Log.d("MIGUE", "DDDD : " + currentUser.toString());
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_chat)
        configureActionBar(
            this.localClassName.toString(),
            ColorDrawable(ContextCompat.getColor(applicationContext,R.color.primary)) //getColor(R.color.primary))
        )

        bottom_navigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.navigation_people -> {
                    replaceFragment(PeopleFragment())
                    true
                }

                R.id.navigation_my_account -> {
                    replaceFragment(ProfileFragment())
                    true
                }
                else ->{
                    false
                }

            }
        }

        Toast.makeText(applicationContext,"EXISTE" + currentUser.toString(),Toast.LENGTH_SHORT)

    }

    private fun replaceFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_layout,fragment)
            commit()
        }
    }

    override fun configureActionBar(title: String, colorBar: ColorDrawable) {
        val actionbar = supportActionBar
        actionbar!!.title = title
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setBackgroundDrawable(colorBar)
    }

    override fun changueOrientation() {
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}
