package com.nanliao.weatherapp.views.activities

import android.content.pm.ActivityInfo
import android.drm.DrmStore
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import com.nanliao.weatherapp.R
import com.nanliao.weatherapp.models.Hero
import com.nanliao.weatherapp.presenters.cloud.CloudHeroe
import kotlinx.android.synthetic.main.activity_dota.*
import com.nanliao.weatherapp.views.activities.adapters.HeroAdapter
import com.nanliao.weatherapp.views.util.Interfaces.ActionBar
import com.nanliao.weatherapp.views.util.Interfaces.Orientation
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class DotaActivity : AppCompatActivity(), ActionBar, Orientation {

    var heroAdapter: HeroAdapter = HeroAdapter()
    var compositeDisposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changueOrientation()
        setContentView(R.layout.activity_dota)
        configureActionBar(
            resources.getString(R.string.dotaActivity_name),
            ColorDrawable(resources.getColor(R.color.primary))
        )

        id_text_login.text = intent.getStringExtra("TAG")
        compositeDisposable = CompositeDisposable()

        search_hero.setIconifiedByDefault(false)
        getData()

        heroes_refresh_layout.setOnRefreshListener {
            getData()
        }


        recyclerview_heroes.layoutManager = LinearLayoutManager(this)
        recyclerview_heroes.adapter = heroAdapter

        search_hero.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                heroAdapter.filter.filter((newText))
                return true
            }

        }


        )

    }

    private fun getData() {

        compositeDisposable!!.add(
            CloudHeroe.getHeroRemote()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    heroes_refresh_layout.isRefreshing = true
                    recyclerview_heroes.visibility = View.GONE
                }
                .subscribe(
                    { listHeroes: MutableList<Hero>? ->
                        if (!listHeroes.isNullOrEmpty()) {
                            heroAdapter.heroFeed = listHeroes
                            heroAdapter.heroFeedSearched.addAll(heroAdapter.heroFeed)
                            heroAdapter.notifyDataSetChanged()
                        }
                        Log.d("ROBIN", "SE TRAJERON LOS DATOS")

                    },
                    {
                        it.printStackTrace()// Toast.makeText(this, "Errorcillo",Toast.LENGTH_SHORT).show()
                    },
                    {
                        recyclerview_heroes.visibility = View.VISIBLE
                        heroes_refresh_layout.isRefreshing = false
                        Log.d("ROBIN", "ONCOMPLETE")
                    }
                )

        )

    }

    override fun onDestroy() {
        super.onDestroy()
        if (compositeDisposable != null && compositeDisposable!!.isDisposed) {
            compositeDisposable!!.dispose()
            compositeDisposable = null
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun changueOrientation() {
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    override fun configureActionBar(title: String, colorBar: ColorDrawable) {
        val actionbar = supportActionBar
        actionbar!!.title = title
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setBackgroundDrawable(colorBar)
    }


}







