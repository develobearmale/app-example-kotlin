package com.nanliao.weatherapp.views.util.Interfaces

import android.graphics.drawable.ColorDrawable

interface ActionBar {

    /*
        val actionbar = supportActionBar
        actionbar!!.title = "Heroes Dota2"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.primary)))
     */

    fun configureActionBar(title:String,colorBar:ColorDrawable)
}